let s:indicator_quickfix = get(g:, 'lightline#lists#indicator_quickfix', 'Q: ')
let s:indicator_location = get(g:, 'lightline#lists#indicator_location', 'L: ')

function! lightline#lists#quickfix() abort
    let l:count = len(getqflist())
    return l:count == 0 ? '' : printf(s:indicator_quickfix . '%d', l:count)
endfunction

function! lightline#lists#location() abort
    let l:count = len(getloclist(win_getid()))
    return l:count == 0 ? '' : printf(s:indicator_location . '%d', l:count)
endfunction
